<?php
/**
 * @file
 * feature_general.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function feature_general_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template__panel_context_7bd032b3-d1ef-4fca-8ad3-e904626d00e8';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'general';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'mnb_firstpage_top_fluid' => NULL,
      'mnb_firstpage_top' => NULL,
      'mnb_firstpage_center_fluid' => NULL,
      'mnb_firstpage_center' => NULL,
      'mnb_firstpage_footer_fluid' => NULL,
      'main_header' => NULL,
      'main_header_menu' => NULL,
      'main_sidebar_first' => NULL,
      'main_sidebar_second' => NULL,
      'main_content' => NULL,
      'main_footer' => NULL,
    ),
    'main_header' => array(
      'style' => 'naked',
    ),
    'main_header_menu' => array(
      'style' => 'naked',
    ),
    'main_sidebar_first' => array(
      'style' => 'naked',
    ),
    'main_content' => array(
      'style' => 'naked',
    ),
    'main_sidebar_second' => array(
      'style' => 'naked',
    ),
    'main_footer' => array(
      'style' => 'default',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'a26f0466-60c3-4173-ab57-ff1af7f3bc22';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'site_template__panel_context_7bd032b3-d1ef-4fca-8ad3-e904626d00e8';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d09c5d20-39e9-469b-93e4-c2fcb1f81875';
  $pane->panel = 'main_content';
  $pane->type = 'panels_mini';
  $pane->subtype = 'header_for_main_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd09c5d20-39e9-469b-93e4-c2fcb1f81875';
  $display->content['new-d09c5d20-39e9-469b-93e4-c2fcb1f81875'] = $pane;
  $display->panels['main_content'][0] = 'new-d09c5d20-39e9-469b-93e4-c2fcb1f81875';
  $pane = new stdClass();
  $pane->pid = 'new-a1fe94e2-9d80-4925-aec6-848bfceec9d7';
  $pane->panel = 'main_content';
  $pane->type = 'page_content';
  $pane->subtype = 'page_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_page_content_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'content-main-wrapper',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a1fe94e2-9d80-4925-aec6-848bfceec9d7';
  $display->content['new-a1fe94e2-9d80-4925-aec6-848bfceec9d7'] = $pane;
  $display->panels['main_content'][1] = 'new-a1fe94e2-9d80-4925-aec6-848bfceec9d7';
  $pane = new stdClass();
  $pane->pid = 'new-a80afad3-d2b1-4231-8d84-670dc0078b98';
  $pane->panel = 'main_footer';
  $pane->type = 'block';
  $pane->subtype = 'block-5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'style' => 'wrapper_raw',
    'settings' => array(
      'title' => array(
        'prefix' => '',
        'suffix' => '',
      ),
      'content' => array(
        'prefix' => '<div class="region-footer">
  <div class="block block-copyright">',
        'suffix' => '  </div>
</div>',
      ),
      'theme' => 0,
    ),
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a80afad3-d2b1-4231-8d84-670dc0078b98';
  $display->content['new-a80afad3-d2b1-4231-8d84-670dc0078b98'] = $pane;
  $display->panels['main_footer'][0] = 'new-a80afad3-d2b1-4231-8d84-670dc0078b98';
  $pane = new stdClass();
  $pane->pid = 'new-439356bf-38e2-4695-b228-396811ddb188';
  $pane->panel = 'main_header';
  $pane->type = 'page_logo';
  $pane->subtype = 'page_logo';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'img-logo',
    'css_class' => 'img-logo',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '439356bf-38e2-4695-b228-396811ddb188';
  $display->content['new-439356bf-38e2-4695-b228-396811ddb188'] = $pane;
  $display->panels['main_header'][0] = 'new-439356bf-38e2-4695-b228-396811ddb188';
  $pane = new stdClass();
  $pane->pid = 'new-0269b612-417e-44a8-b117-6f22deca0a0e';
  $pane->panel = 'main_header';
  $pane->type = 'page_slogan';
  $pane->subtype = 'page_slogan';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'slogan',
    'css_class' => 'slogan',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0269b612-417e-44a8-b117-6f22deca0a0e';
  $display->content['new-0269b612-417e-44a8-b117-6f22deca0a0e'] = $pane;
  $display->panels['main_header'][1] = 'new-0269b612-417e-44a8-b117-6f22deca0a0e';
  $pane = new stdClass();
  $pane->pid = 'new-e0a2a6a8-a0cf-447d-9d37-94be78a6fe5b';
  $pane->panel = 'main_header_menu';
  $pane->type = 'page_logo';
  $pane->subtype = 'page_logo';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'isStuck-logo',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e0a2a6a8-a0cf-447d-9d37-94be78a6fe5b';
  $display->content['new-e0a2a6a8-a0cf-447d-9d37-94be78a6fe5b'] = $pane;
  $display->panels['main_header_menu'][0] = 'new-e0a2a6a8-a0cf-447d-9d37-94be78a6fe5b';
  $pane = new stdClass();
  $pane->pid = 'new-edbc3804-71b5-47a0-8336-be39ca5d980e';
  $pane->panel = 'main_header_menu';
  $pane->type = 'block';
  $pane->subtype = 'superfish-1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'span',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'region-menu',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'edbc3804-71b5-47a0-8336-be39ca5d980e';
  $display->content['new-edbc3804-71b5-47a0-8336-be39ca5d980e'] = $pane;
  $display->panels['main_header_menu'][1] = 'new-edbc3804-71b5-47a0-8336-be39ca5d980e';
  $pane = new stdClass();
  $pane->pid = 'new-5a71845e-eb5f-4350-b3ec-f61688f5847a';
  $pane->panel = 'main_header_menu';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Load mobile menu',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<script>
jQuery(\'#superfish-1\').slicknav({
        prependTo: \'.header-section-2 .grid-12\',
        allowParentLinks: true,
        parentTag: \'span\'
 });
</script>
',
    'format' => 'php_code',
    'substitute' => 1,
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '604800',
      'granularity' => 'none',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '5a71845e-eb5f-4350-b3ec-f61688f5847a';
  $display->content['new-5a71845e-eb5f-4350-b3ec-f61688f5847a'] = $pane;
  $display->panels['main_header_menu'][2] = 'new-5a71845e-eb5f-4350-b3ec-f61688f5847a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template__panel_context_7bd032b3-d1ef-4fca-8ad3-e904626d00e8'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function feature_general_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = '404_page_not_found';
  $page->task = 'page';
  $page->admin_title = '404 Page not found';
  $page->admin_description = '';
  $page->path = 'page-404';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_404_page_not_found_panel_context';
  $handler->task = 'page';
  $handler->subtask = '404_page_not_found';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'page-404',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
    'right' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd773c1f6-4f1f-41da-9100-ce3436798978';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_404_page_not_found_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7b753e26-6e3a-418b-bb4c-e62ed01d7f09';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '404',
    'title' => '',
    'body' => '<span class="404-num">404</span>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7b753e26-6e3a-418b-bb4c-e62ed01d7f09';
  $display->content['new-7b753e26-6e3a-418b-bb4c-e62ed01d7f09'] = $pane;
  $display->panels['left'][0] = 'new-7b753e26-6e3a-418b-bb4c-e62ed01d7f09';
  $pane = new stdClass();
  $pane->pid = 'new-7f865dda-6817-4cc0-9d9a-d06861cc0f1a';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Упс',
    'title' => 'Упс!',
    'body' => '<h4>Страница не найдена</h4>
Это могло произойти по следующим причинам:
- страница была удалена или переименована,
- страница никогда не существовала,
- у Вас была неправильная ссылка. 

Что делать в этом случае? 

- перейдите на <a href="/">Главную страницу</a>
- воспользуйтесь Поиском,
- сообщите Админу о битой ссылке. Спасибо!',
    'format' => 'full_html',
    'substitute' => TRUE,
    'title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7f865dda-6817-4cc0-9d9a-d06861cc0f1a';
  $display->content['new-7f865dda-6817-4cc0-9d9a-d06861cc0f1a'] = $pane;
  $display->panels['right'][0] = 'new-7f865dda-6817-4cc0-9d9a-d06861cc0f1a';
  $pane = new stdClass();
  $pane->pid = 'new-15dc8ad1-43f2-46e3-984f-4667b423d4c9';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'search-form';
  $pane->shown = TRUE;
  $pane->access = array(
    'logic' => 'and',
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Search',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '15',
      'granularity' => 'none',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '404-search',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array(
    'type' => 'none',
    'regions' => array(
      'right' => 'right',
    ),
  );
  $pane->uuid = '15dc8ad1-43f2-46e3-984f-4667b423d4c9';
  $display->content['new-15dc8ad1-43f2-46e3-984f-4667b423d4c9'] = $pane;
  $display->panels['right'][1] = 'new-15dc8ad1-43f2-46e3-984f-4667b423d4c9';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['404_page_not_found'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'about';
  $page->task = 'page';
  $page->admin_title = 'About';
  $page->admin_description = '';
  $page->path = 'about';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'About',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_about_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'about';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'page-about',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => TRUE,
        'fixed_width' => '',
        'column_separation' => '0',
        'region_separation' => '0',
        'row_separation' => '0',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7f5c8b9b-100d-49c0-86cc-b6d425d452d1';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_about_panel_context';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['about'] = $page;

  $page = new stdClass();
  $page->disabled = TRUE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'contacts';
  $page->task = 'page';
  $page->admin_title = 'Contacts';
  $page->admin_description = '';
  $page->path = 'contacts';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Contacts',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_contacts_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'contacts';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'page-contacts',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => TRUE,
        'fixed_width' => '',
        'column_separation' => '0',
        'region_separation' => '0',
        'row_separation' => '0',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
          1 => 1,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
        'class' => 'pannel-map',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'contacts',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'contacts' => array(
        'type' => 'region',
        'title' => 'Contacts',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'contact_form' => NULL,
      'contact_information' => NULL,
      'contacts' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '63da6deb-cc5a-4a9c-bc5e-61a8d723fbc4';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_contacts_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e7883f96-6219-47f9-94a4-1612300cd448';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Our Location',
    'title' => 'Our Location',
    'body' => '[gmap lat_coord="55.861263" lng_coord="-4.251604" zoom_value="15" zoom_wheel="no"][/gmap]',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e7883f96-6219-47f9-94a4-1612300cd448';
  $display->content['new-e7883f96-6219-47f9-94a4-1612300cd448'] = $pane;
  $display->panels['center'][0] = 'new-e7883f96-6219-47f9-94a4-1612300cd448';
  $pane = new stdClass();
  $pane->pid = 'new-81e7acac-959f-4fd6-9252-da9230ea95f5';
  $pane->panel = 'contacts';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Contact Info',
    'body' => '<h4>Sed ut perspiciatis unde omnis iste natus error sit.</h4>
Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque et quasi architecto beatae vitae dicta sunt explicabo. Mornunc odio gravida atcursus neus a lorem. Maecenas tristique orci ac sem. Duis ultric pharetra magna.</p>

<strong>The Company Name Inc.<br />
9870 St Vincent Place,<br />
Glasgow, DC 45 Fr 45.</strong>
Telephone: +1 800 603 6035<br />
FAX: +1 800 889 9898<br />
E-mail: <a href="mailto:mail@demolink.org">mail@demolink.org</a>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'col-1-3',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '81e7acac-959f-4fd6-9252-da9230ea95f5';
  $display->content['new-81e7acac-959f-4fd6-9252-da9230ea95f5'] = $pane;
  $display->panels['contacts'][0] = 'new-81e7acac-959f-4fd6-9252-da9230ea95f5';
  $pane = new stdClass();
  $pane->pid = 'new-6f827c5f-ed0d-4941-b52a-cdb9ea437387';
  $pane->panel = 'contacts';
  $pane->type = 'contact';
  $pane->subtype = 'contact';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Contact Form',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'col-2-3 col-last',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '6f827c5f-ed0d-4941-b52a-cdb9ea437387';
  $display->content['new-6f827c5f-ed0d-4941-b52a-cdb9ea437387'] = $pane;
  $display->panels['contacts'][1] = 'new-6f827c5f-ed0d-4941-b52a-cdb9ea437387';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['contacts'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'Home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'main';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => TRUE,
        'fixed_width' => '',
        'column_separation' => '0',
        'region_separation' => '0',
        'row_separation' => '0',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '71f4e7ce-6575-42a8-9544-270e27c92f27';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_home_panel_context';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  $page = new stdClass();
  $page->disabled = TRUE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'privacy_policy';
  $page->task = 'page';
  $page->admin_title = 'Privacy Policy';
  $page->admin_description = '';
  $page->path = 'privacy-policy';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_privacy_policy_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'privacy_policy';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => TRUE,
        'fixed_width' => '',
        'column_separation' => '0',
        'region_separation' => '0',
        'row_separation' => '0',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Privacy Policy';
  $display->uuid = '2aa31d27-3ea6-4d6f-98d9-ea7e466dfd92';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_privacy_policy_panel_context_2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-88f0c656-b546-4bcb-a04c-12fe21586f5d';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong> Sed non arcu enim. Nam sed magna magna, eget tempus neque. Donec lectus enim, auctor ut dignissim sit amet, auctor aliquam tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus a lectus id nibh mattis ultrices. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis suscipit eleifend mauris non condimentum. Mauris iaculis, nibh id interdum hendrerit, turpis turpis placerat dui, ac mattis augue quam eu tortor. Aenean condimentum feugiat rhoncus. Vestibulum id nulla auctor eros vehicula pellentesque.</p>
<p><strong>Proin nec nibh erat, ut interdum tortor.</strong> Aliquam lectus risus, hendrerit vitae mollis et, molestie nec turpis. Nullam gravida diam sed velit dignissim bibendum. Cras tristique mi eu tortor aliquet a blandit urna tempor. Maecenas adipiscing euismod sagittis. Suspendisse vulputate convallis tellus, sed mattis justo faucibus ac. Phasellus non diam ut nisl viverra sodales. Aenean at nunc quis tortor consequat scelerisque. In dignissim, ante vel imperdiet venenatis, felis nisi imperdiet lectus, et ultrices eros justo in lacus. Quisque et ante ac metus eleifend laoreet. Cras leo metus, pharetra et molestie quis, iaculis ut felis.</p>
<p><strong>Nullam sed viverra libero.</strong> Vestibulum imperdiet fermentum massa, a fringilla tortor varius vel. Praesent ac metus massa, ac dapibus lorem. Nunc cursus nisi eget dui suscipit aliquam viverra odio iaculis. Donec commodo erat non arcu volutpat viverra. Nulla facilisi. Nam sagittis metus sit amet quam porttitor sollicitudin. Mauris aliquam felis at enim gravida porta. Sed ultrices aliquam convallis. Sed non consequat ligula. Sed purus felis, pellentesque quis pretium ut, bibendum at est.</p>
<p><strong>Quisque lacinia iaculis fermentum.</strong> Sed sed ante mauris, at pretium mauris. Mauris leo sem, dictum eget tincidunt at, gravida vel lacus. Nam fringilla tristique lectus. Etiam aliquam odio ac arcu posuere placerat. Nunc ipsum eros, euismod vitae molestie at, euismod ut nisl. Pellentesque a convallis dui. Nunc porttitor, turpis vel faucibus sollicitudin, dui nunc placerat risus, non eleifend nulla risus at diam. Integer eget dui dui, ut porta felis. Nam nisi nisl, eleifend a pulvinar nec, pulvinar vitae lorem. In tristique egestas ligula. Mauris sed dui nec leo molestie tempor. Praesent pellentesque molestie magna, a venenatis orci viverra in. Donec eget dui sit amet tortor ornare semper non et est. Suspendisse a orci ut velit consectetur placerat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas lacinia tincidunt leo in tincidunt.</p>
<p><strong>Fusce lacus tortor, venenatis id fringilla convallis, interdum nec elit.</strong> Sed viverra, felis in laoreet ultrices, leo ligula molestie augue, vel tincidunt turpis mi vel orci. Nunc dolor orci, ultricies sed vestibulum sed, cursus nec risus. Duis augue risus, ullamcorper vel condimentum cursus, aliquam nec risus. Nunc id mauris in massa lacinia bibendum. Mauris lacinia sagittis adipiscing. Maecenas gravida sapien at magna semper dignissim a vel erat. Phasellus nec risus vel augue bibendum bibendum. Nullam posuere libero dui, id gravida erat. Morbi est tortor, auctor a iaculis ullamcorper, gravida eget magna. Nam aliquam enim non justo volutpat eget aliquet diam iaculis. Cras dui eros, consectetur eu mattis eleifend, laoreet in turpis. Curabitur eu porta lacus.</p>
Email: <a href="mailto:admin@demolink.org">admin@demolink.org</a>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '88f0c656-b546-4bcb-a04c-12fe21586f5d';
  $display->content['new-88f0c656-b546-4bcb-a04c-12fe21586f5d'] = $pane;
  $display->panels['center'][0] = 'new-88f0c656-b546-4bcb-a04c-12fe21586f5d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['privacy_policy'] = $page;

  $page = new stdClass();
  $page->disabled = TRUE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'services';
  $page->task = 'page';
  $page->admin_title = 'Services';
  $page->admin_description = '';
  $page->path = 'services';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Services',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_services_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'services';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'page-services',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => TRUE,
        'fixed_width' => '',
        'column_separation' => '0',
        'region_separation' => '0',
        'row_separation' => '0',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '20128f43-b250-4e96-bc90-f692c2eba349';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_services_panel_context';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['services'] = $page;

  return $pages;

}
