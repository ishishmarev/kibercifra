<?php
/**
 * @file
 * feature_general.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function feature_general_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'header_for_main_content';
  $mini->category = '';
  $mini->admin_title = 'Header for main content';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => array(
        'content' => array(
          'prefix' => '<header id="content-header" class="content-header">',
          'suffix' => '</header>',
        ),
        'theme' => 0,
      ),
    ),
    'middle' => array(
      'style' => 'wrapper_raw',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'fd682ccb-00c1-495b-8867-764ba521c8df';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'header_for_main_content';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bc4936fb-39f2-4447-ad4c-c15dcf16ba14';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'messages',
    'css_class' => 'messages-wrapper clearfix',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bc4936fb-39f2-4447-ad4c-c15dcf16ba14';
  $display->content['new-bc4936fb-39f2-4447-ad4c-c15dcf16ba14'] = $pane;
  $display->panels['middle'][0] = 'new-bc4936fb-39f2-4447-ad4c-c15dcf16ba14';
  $pane = new stdClass();
  $pane->pid = 'new-9da223fc-180d-485c-8f19-940a8dbbc899';
  $pane->panel = 'middle';
  $pane->type = 'page_title';
  $pane->subtype = 'page_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'markup' => 'h2',
    'class' => 'title page-title',
    'id' => 'page-title',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9da223fc-180d-485c-8f19-940a8dbbc899';
  $display->content['new-9da223fc-180d-485c-8f19-940a8dbbc899'] = $pane;
  $display->panels['middle'][1] = 'new-9da223fc-180d-485c-8f19-940a8dbbc899';
  $pane = new stdClass();
  $pane->pid = 'new-085b081d-010e-4030-ace7-fed312523582';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'system-help';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '085b081d-010e-4030-ace7-fed312523582';
  $display->content['new-085b081d-010e-4030-ace7-fed312523582'] = $pane;
  $display->panels['middle'][2] = 'new-085b081d-010e-4030-ace7-fed312523582';
  $pane = new stdClass();
  $pane->pid = 'new-ea4b70cf-eef2-416e-8a42-017bdaefdcb2';
  $pane->panel = 'middle';
  $pane->type = 'page_tabs';
  $pane->subtype = 'page_tabs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'primary',
    'id' => 'tabs',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'ea4b70cf-eef2-416e-8a42-017bdaefdcb2';
  $display->content['new-ea4b70cf-eef2-416e-8a42-017bdaefdcb2'] = $pane;
  $display->panels['middle'][3] = 'new-ea4b70cf-eef2-416e-8a42-017bdaefdcb2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['header_for_main_content'] = $mini;

  return $export;
}
