<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *	 panel of the layout. This layout supports the following sections:
 *	 - $content['left']: Content in the left column.
 *	 - $content['right']: Content in the right column.
 */
?>
<div class="page-wrapper <?php !empty($class) ? print $class : ''; ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : ''; ?>>
  <div id="page" class="page">

    <!-- Header
        ======================================================================================= -->
    <header id="header" class="header page-header clearfix" role="banner">

      <div class="header-section-wrapper">
        <div class="header-section-1">
          <div class="container-12">
            <div class="grid-12">
              <div id="logo" class="logo">
                <?php print $content['main_header']; ?>
              </div>
            </div>
          </div>
        </div>


        <div class="<?php if ( theme_get_setting( 'kibercifra_sticky_menu' ) ) { echo 'stickup '; } ?>header-section-2"> <!-- Sticky menu wrapper -->
          <div class="container-12">
            <div class="grid-12">
              <?php print $content['main_header_menu']; ?>
            </div>
          </div>
        </div>
      </div>

    </header>

    <!-- Content
		======================================================================================= -->
    <div id="main-wrapper" class="main-wrapper" role="main">
      <div class="container-12">
        <div class="grid-12">
          <div id="main" class="main clearfix">

            <?php if ( $content['main_sidebar_first'] ) : ?>
              <!-- Left sidebar -->
              <aside id="sidebar-first" class="sidebar-first sidebar grid-4 alpha" role="complementary">
                  <?php print render( $content['main_sidebar_first'] ); ?>
              </aside>
            <?php endif; ?>

            <!-- Page content -->
            <div id="content" class="content content-main <?php if ( ( $content['main_sidebar_first'] ) && ( $content['main_sidebar_second'] ) ) : print 'grid-4'; elseif ( $content['main_sidebar_first'] ) : print 'grid-8 omega'; elseif ( $content['main_sidebar_second'] ) : print 'grid-8 alpha'; endif; ?>">

              <!-- Page content -->
                <?php if ( $content['main_content'] ) :
                    print render( $content['main_content'] );
                endif; ?>
            </div>

            <?php if ( $content['main_sidebar_second'] ) : ?>
              <!-- Right sidebar -->
              <aside id="sidebar-second" class="sidebar-second sidebar grid-4 omega" role="complementary">
                <div class="section">
                    <?php print render( $content['main_sidebar_second'] ); ?>
                </div>
              </aside>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>

    <!-- Footer
    ======================================================================================= -->
    <footer id="footer" class="footer page-footer" role="contentinfo">
      <!-- Region Footer top -->
      <div class="footer-wrapper">
        <div class="container-12">
          <div class="grid-12 clearfix">
            <!-- Region Footer -->
            <?php if ( $content['main_footer'] ) :
              print render( $content['main_footer'] );
            endif; ?>
          </div>
        </div>
      </div>
    </footer>

  </div>
</div>