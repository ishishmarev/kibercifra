
<?php

// Plugin definition
$plugin = array(
    'title' => t('General layout'),
    'category' => t('Custom layouts'),
    'icon' => 'general.png',
    'theme' => 'general',
    'regions' => array(
        'main_header' => t('Main head'),
        'main_header_menu' => t('Main head menu'),
        'main_sidebar_first' => t('Main sidebar first'),
        'main_sidebar_second' => t('Main sidebar second'),
        'main_content' => t('Main content'),
        'main_footer' => t('Main footer'),
    )
);